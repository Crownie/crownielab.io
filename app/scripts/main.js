/* eslint-disable no-unused-vars, no-trailing-spaces */
var ScrollMagic = ScrollMagic || {};
var $ = $ || {};
var Swiper = Swiper || {};
var TweenMax = TweenMax || {};
var TimelineMax = TimelineMax || {};
(function () {
    'use strict';

    function styleStringToObject(styleString) {
        var arr = styleString.split(/;/g);
        var pat = /(\w+):(.+)$/;
        var styleObj = {};

        for (var i = 0, j = arr.length; i < j; i++) {
            var matches = pat.exec(arr[i]);
            if (matches !== undefined) {
                styleObj[matches[1]] = matches[2];
            }
        }
        return styleObj;
    }

    $(function () {

        $('a[rel="scroll"]').click(function (e) {

            if (location.hostname === this.hostname) {
                e.preventDefault();
                var target = $(this.hash);

                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });


        var controller = new ScrollMagic.Controller({});

        var aboutPicTween1 = TweenMax.to('#about-pic', 0.5, {opacity: '1'});
        var aboutPicTween2 = TweenMax.to('#about-pic', 0.5, {opacity: '0'});

        var scene = new ScrollMagic.Scene({triggerElement: '#about-me', duration: 1200})
            .setPin('#about-me')
            //  .addIndicators({ name: '2 (duration: 0)' })
            .triggerHook('onLeave')
            .addTo(controller);

        scene = new ScrollMagic.Scene({triggerElement: '#about-me', duration: 500})
            .setTween(aboutPicTween1)
            //  .addIndicators()
            .addTo(controller);

        scene = new ScrollMagic.Scene({triggerElement: '#about-me', duration: 300})
            .setTween(aboutPicTween2)
            //   .addIndicators()
            .triggerHook(0)
            .offset(600)
            .addTo(controller);

        //animate about text
        $('#about-me .row').each(function (i, e) {
            var targetStyleString = this.dataset.to;

            var offset = this.dataset.offset || 0;

            if (this.dataset.to == null) {
                return;
            }
            var targetStyleObj = styleStringToObject(targetStyleString);

            var tween = new TimelineMax()
                .to(this, 0.5, targetStyleObj)
                .to(this, 0.5, {opacity: 0});
            var scene1 = new ScrollMagic.Scene({triggerElement: '#about-me', duration: 400})
                .setTween(tween)
                .triggerHook('onLeave')
                .offset(offset)
                //   .addIndicators()
                .addTo(controller);
        });

        //quick fix for section height
        $('#about-me').css('height', window.innerHeight + 'px');
        $(window).resize(function () {
            $('#about-me').css('height', window.innerHeight + 'px');
        });

        $.get('data/projects.json', function (obj) {

            var counter = 0;
            if (typeof obj === 'string') {
                obj = JSON.parse(obj);
            }

            for (var groupName in obj) {

                var groupId = $.trim(groupName.toLowerCase()).replace(/\s+/g, '-');
                var li = document.createElement('li');
                var a = document.createElement('a');
                if (counter === 0) {
                    li.setAttribute('class', 'active');
                }
                a.setAttribute('href', '#p-' + groupId);
                a.setAttribute('role', 'tab');
                a.setAttribute('data-toggle', 'tab');
                a.textContent = groupName;

                li.appendChild(a);
                $('#portfolio .tabs').append(li);

                var div = document.createElement('div');
                div.setAttribute('id', 'p-' + groupId);
                div.setAttribute('class', 'tab-pane fade in');
                if (counter === 0) {
                    div.classList.add('active');
                }


                div.appendChild($($('#swiper-template').html())[0]);
                $('#portfolio .tab-content').append(div);

                var projects = obj[groupName];

                for (var d in projects) {
                    var {title} = projects[d];
                    var {image} = projects[d];
                    var {description} = projects[d];
                    var {playstore} = projects[d];
                    var {website} = projects[d];
                    var itemTemplate = document.getElementById('project-item-template').innerHTML;
                    itemTemplate = itemTemplate
                        .replace(/<%title%>/g, title)
                        .replace(/<%image_url%>/g, image)
                        .replace('<%description%>', description)
                        .replace('<%playstore%>', playstore)
                        .replace('<%website%>', website);
                    $('#p-' + groupId + ' .swiper-wrapper').append(itemTemplate);
                    // $('#p-' + groupId + ' .swiper-container').attr('id',);
                }


                counter++;
            }

            var swiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                effect: 'coverflow',
                grabCursor: true,
                centeredSlides: false,
                slidesPerView: '3',
                coverflow: {
                    rotate: 50,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: true
                }
            });


            $('#portfolio .tabs li').click(function (e) {
                var index = $(this).index();
                setTimeout(function () {
                    swiper[index].update();
                }, 200);
            });
        });

        $('#portfolioModal').on('show.bs.modal', function (e) {
            var markup = $(e.relatedTarget).find('.modal-data').html();
            $('#portfolioModal .modal-content .modal-inner').html('').html(markup);
        });

        $('#contact-form').submit(function (e) {
            e.preventDefault();
            var $form = $(this),
                emailRegExp=/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

            var name = $form.find('input[name="name"]').val();
            var email = $form.find('input[name="email"]').val();
            var message = $form.find('textarea[name="message"]').val();

            var missingFields = [];

            console.log($form.find('input[name="name"]'));
            console.log({name: name, email: email, message: message});

            if($.trim(name).length<=0){
                missingFields.push("Name");
            }
            if($.trim(email).length<=0){
                missingFields.push("Email");
            }
            if($.trim(message).length<=0){
                missingFields.push("Message");
            }

            if(missingFields.length>0){
                $('body').overhang({
                    type: 'error',
                    message: "Please fill in the required field"+((missingFields.length>1)?'s':'')+'; '+missingFields.join(', '),
                    closeConfirm: true
                });

                return;
            }

            if(!emailRegExp.test(email)){
                $('body').overhang({
                    type: 'error',
                    message: 'Please enter a valid email address',
                    closeConfirm: true
                });
                return;
            }

            $.ajax({
                url: $(this).attr('action'),
                method: "POST",
                data: {name: name, email: email, message: message},
                dataType: "json"
            }).done(function(response){
                $("body").overhang({
                    type: "success",
                    message: response.success,
                    duration: 5
                });
                $form.trigger('reset');
            });

        });


    });


})();
