<?php
session_start();

function respond($status, $data = "") {
	echo json_encode(array('status' => $status, 'data' => $data));
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {

	include_once '../securimage/securimage.php';
	$securimage = new Securimage();

	if ($securimage -> check($_POST['captchaCode']) == false) {
		// the captcha code was incorrect
		respond(0, " captcha error. The captcha code you entered is not correct.");
		exit ;
	}

	$name = $_POST['name'];
	$email = $_POST['email'];
	$subject = 'New msg from Crownie.tk';
	$message = $_POST['message'];

	$error = "";
	if (ctype_space($name) || $name == "")
		$error .= "invalid name<br>";
	if (!filter_var($email, FILTER_VALIDATE_EMAIL))
		$error .= "invalid email<br>";
	if (ctype_space($message) || $message == "")
		$error .= "invalid message<br>";

	if ($error !== "") {
		respond(0, $error);
		exit ;
	}
	$message = "Name: " . $name . "\n\n" . $message;

	$to = "steveayilara@gmail.com";
	$header = "From:$email \r\n";
	$retval = mail($to, $subject, $message . "\r\n sent from the website contact form", $header);
	if ($retval == true) {
		respond(1, "sent");
	} else {
		respond(0, "error sending message");
	}
}
?>